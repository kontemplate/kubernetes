some-service configuration
==========================

This is the resource set for a theoretical 'some-service' service. The
most interesting / important part in here is that the `image` field in
the deployment is templated using the `version` parameter which is
defined in `default.json`.

This value in `default.json` is the one updated by the hook for the
update script.
