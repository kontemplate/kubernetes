Kubernetes / Kontemplate configuration
======================================

This repository contains the [kontemplate][] configuration for a
fictional example cluster.

It is part of the kontemplate [example project][] for demonstrating a
Kontemplate-based automated deployment flow using Gitlab CI.

------------

## Overview

Every top-level `*-cluster.yaml` file defines the Kontemplate
configuration for an entire Kubernetes cluster, this includes all the
resource sets and their configuration that are to be deployed into
this cluster.

When a project repository (example TBD) is updated, it publishes a new
Docker image and - as the last step of its CI pipeline - calls the
Gitlab CI hook for the [update script][] project.

The update script will then update the `default.json` file for the
relevant resource set with the new image tag and the CI pipeline of
**this** repository will call `kontemplate apply ...` on the clusters
to perform the changes. (see [.gitlab-ci.yml](.gitlab-ci.yml) for
details).

[kontemplate]: http://kontemplate.works/
[example project]: https://gitlab.com/kontemplate/
[update script]: https://gitlab.com/kontemplate/k8s-update-hook/
